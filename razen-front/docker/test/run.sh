#!/bin/bash

echo "Run Jest on $1/js/..."
(
    cd $1/js/
    jest
)
if [ $? -ne 0 ]; then exit 1; fi
