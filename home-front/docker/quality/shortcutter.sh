#! /bin/bash
# @file shortcutter.sh
# Just a little tool to generate shortcuts to node scripts

if [ $# -ne 2 ]
then
    echo "Invalid use of shortcutter"
    exit 1
fi

cat > /usr/bin/$1 <<EOF
#! /bin/bash

/usr/bin/node /node_modules/$2 \$@
EOF

chmod +x /usr/bin/$1
