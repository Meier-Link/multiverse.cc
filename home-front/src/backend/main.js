/**
 * The Backend API
 * @file main.js
 */

/**
 * Fetch static files from the backend as text file.
 * @aram {String} path - of the data to fetch
 * @return {String} The data found as a String
 */
async function fetchStaticAsString(path)
{
    // TODO(JBR) the hostname should be some attribute global to the Backend API
    //let url = `http://faeries.space/static/${path}`;
    //return await (await fetch(url)).text();
    return await (await fetch(path)).text();
}

export { fetchStaticAsString };
