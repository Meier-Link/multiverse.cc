/**
 * Simplified Redux implementation for the needs of the project's start.
 *
 * NB: useful for study purpose, but give a try to the official Redux: redux.js.org
 */
'use strict';

import { md5 } from './md5.js';

function createStore(initState)
{
    let state = {};

    function _hasKey(key)
    {
        return Object.prototype.hasOwnProperty.call(state, key);
    }

    function _reducer(key, val)
    {
        if (_hasKey(key))
        {
            /*for (let follower of state[key].followers)
            {
                // TODO(JBR) How to make sure a follower fail doesn't break update
                // of the key?
                follower(key, state[key].payload, val);
            }*/
            const followers = state[key].followers;
            Object.keys(followers).forEach(follower => {
                // TODO(JBR) How to make sure a follower fail doesn't break update
                // of the key?
                followers[follower](key, state[key].payload, val);
            });
            state[key].payload = val;
        }
    }

    /**
     * Get either the state of the whole keys or the state of the given key.
     * @param key {String} - which key to look for (optional - no default)
     * @return {misc} either the data corresponding to the key or an array with
     *  all the keys and associated values.
     */
    function getState(key)
    {
        if (key) {
            if (_hasKey(key))
            {
                return state[key].payload;
            }
            else
            {
                return null;
            }
        }
        let kvs = {};
        Object.keys(state).forEach(x => kvs[x] = state[x].payload);
        return kvs;
    }

    /**
     * Unsubscribe to a given key.
     */
    function _unsubscribe(key, callbackIdx, firstTime)
    {
        delete state[key].payload[callbackIdx];
    }

    /**
     * Make possible to register new items in the state
     */
    function register(ext)
    {
        for (let key of ext)
        {
            if (!_hasKey(key))
            {
                state[key] = {followers: {}, payload: null};
            }
        }
    }

    /**
     * Subscribe to the changes on a given key
     */
    function subscribe(keyVal, callback)
    {
        // TODO(JBR) Make sure callback 'is' a callback.
        if (!_hasKey(keyVal))
        {
            register([keyVal]);
        }
        let idx = md5(callback.toString());  // TODO(JBR) to test.
        state[keyVal].followers[idx] = callback;
        return {
            unsubscribe: () => {
                _unsubscribe(keyVal, idx);
            }
        };
    }

    /**
     * Trigger the given callback instantly, then register it to be triggered
     * whenevre the value of the given state changes.
     * @param {String} key of the state to follow.
     * @param {Callable[obj]} callback (with the state's current value as parameter)
     *  to trigger for the given state.
     * @return The subscription of the callback.
     */
    function trigger(key, callback)
    {
        let fn = (key, oldVal, newVal) => callback(newVal);
        callback(getState(key));
        return subscribe(key, fn);
    }

    function dispatch(action)
    {
        _reducer(action.key, action.value);
    }

    if (initState)
    {
        let items = Object.keys(initState);
        register(items);
        items.forEach(item => dispatch({ key: item, value: initState[item] }));
    }

    return { getState, register, subscribe, trigger, dispatch };
}

/* Example
 * const exampleStore = createStore();
 * exampleStore.register(['dark-mode', 'username']);
 * exampleStore.subscribe(
 *     'dark-mode',
 *     (key, oldVal, newVal) => console.log('Update the class to ' + newVal));
 * exampleStore.dispatch({key: 'dark-mode', value: 'enabled'});
 */

const store = createStore();

let state = {store: store, createStore: createStore};
export { state };
