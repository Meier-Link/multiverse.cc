/**
 * CustomElementIf
 * Interface the base for mechanics' custom elements.
 */

/* Local references */
import { fetchStaticAsString } from '/backend/main.js';
import { route } from '/mechanics/libs/router.js';
import { state } from '/mechanics/libs/state-store.js';
import { uid } from '/mechanics/libs/uniq-id.js';


class CustomElementIf extends HTMLElement
{
    /** To be overriden */

    /**
     * Follow the updates on the custom element's attributes.
     * The object expects the name of the attribute as a key, and a callback
     * accepting the attribute value as parameter.
     */
    static defaultAttributes = { };

    /**
     * Name of the tag to be registered, or internally used in the Mechanics'
     * custom elements store.
     */
    static tagName = undefined;

    /**
     * Method called at the end of the constructor.
     */
    postConstruction() { }

    /**
     * Called when the content of the custom element is ready.
     * Use it to render dynamic content from attributes, for instance.
     */
    render() { }

    /** Do not override - internal API */

    /**
     * Register an internal state by its name.
     * @param[in] {List(String)} name of the state.
     */
    stateRegister(names)
    {
        let statesName = [];
        names.forEach(name => {
            statesName.push(this.id + '-' + name);
        });
        this._internalState.register(statesName);
    }

    /**
     * Get current value of an internal state.
     * @param[in] {String} the state name.
     * @return {object} the current state value.
     */
    stateGet(name)
    {
        let id = this.id;
        let state = this._internalState.getState(this.id + '-' + name);
        return state;
    }

    /**
     * Subscribe to an internal state.
     * @param[in] {String} state name.
     * @param[in] {function} callback when something append to this state.
     */
    stateSubscribe(name, callback)
    {
        this.subscriptions.push(this._internalState.subscribe(
            this.id + '-' + name,
            callback
        ));
    }

    /**
     * Dispatch an update to an internal state.
     * @param[in] {String} name of the state to update.
     * @param[in] {object} the new state value.
     */
    stateDispatch(name, val)
    {
        this._internalState.dispatch({
            key: this.id + '-' + name,
            value: val
        });
    }

    /**
     * Convenient way to trigger a callback when a given condition is met on
     * the given state, even if the condition is already met.
     * @param {String} Name of the state to follow.
     * @param {function} action to perform when the expectations is met.
     */
    stateTrigger(name, callback)
    {
        callback(this.stateGet(name));
        let fn = (key, oldVal, newVal) => callback(newVal);
        this.stateSubscribe(name, fn);
    }

    /** Do not override */

    /** Internal state of the custom elements. */
    _internalState = state.createStore();

    /**
     * Load the inner html data from static files.
     */
    async loadInnerHTML()
    {
        let tagName = this.constructor.tagName;
        let remoteBaseDir = `mechanics/custom-elements/${tagName}`;
        let cssData = await fetchStaticAsString(`${remoteBaseDir}/style.css`);
        let htmlData = await fetchStaticAsString(`${remoteBaseDir}/index.html`);
        return `<style type="text/css" scoped>
    ${cssData}
</style>
${htmlData}`;
    }

    constructor()
    {
        super();
        /** Subscriptions of this custom element. */
        this.subscriptions = [];
        /** The unique ID of this custom element. */
        this.id = uid();
        let attributesName = Object.keys(this.constructor.defaultAttributes);
        let names = [...(new Set(['content-loaded'].concat(attributesName)))];
        this.stateRegister(names);
        names.forEach(attr => {
            let val = this.constructor.defaultAttributes[attr];
            if (val)
            {
                this.stateDispatch(attr, val);
            }
        });
        this.stateTrigger(
            'content-loaded',
            (val) => {
                if (val)
                {
                    this.render();
                }
            }
        );
        this.loadInnerHTML().then((data) => {
            this.innerHTML = data;
            this.stateDispatch('content-loaded', true);
        });
        this.postConstruction();
    }

    /**
     * Apply changes whenever the attribute attr is updated.
     * @param attr {String} the attribute which has changed.
     * @param oldValue {String} the old value.
     * @param newValue {String} the new value.
     */
    attributeChangedCallback(attr, oldValue, newValue)
    {
        this.stateDispatch(attr, newValue);
    }

    /**
     * Method called whenevere the custom element is instanciated.
     * Here we connect the attributes of the current instance to the content.
     */
    connectedCallback()
    {
        Object.keys(this.constructor.defaultAttributes).forEach((attr) => {
            let htmlAttr = this.getAttribute(attr);
            if (htmlAttr)
            {
                this.stateDispatch(attr, htmlAttr);
            }
        });
    }

    /**
     * Method called whenever the current instance is destroyed.
     */
    disconnectedCallBack() {
        this.subscriptions.forEach((sub) => { if (sub) sub.unsubscribe(); });
    }

    /**
     * Attributes of the current custom element.
     */
    static get observedAttributes() {
        return Object.keys(this.defaultAttributes);
    }
}

export { CustomElementIf };
