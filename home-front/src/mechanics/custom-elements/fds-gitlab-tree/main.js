/**
 * FdsGitlabTree
 * Display the directories' tree of a Gitlab repository.
 */

import { fetchStaticAsString } from '/backend/main.js';
import { CustomElementIf } from  '/mechanics/libs/custom-element-if.js';
import { state } from '/mechanics/libs/state-store.js';
import { route } from '/mechanics/libs/router.js';


class FdsGitlabTree extends CustomElementIf
{
    /**
     * Fetch the tree from the repository.
     */
    async getGitlabTree(urlInfos)
    {
        let repo = urlInfos['repo-host'];
        let project = urlInfos['project-id'];
        let branch = urlInfos['branch'];
        let path = urlInfos['path'];
        let url = `${repo}/api/v4/projects/${project}/repository/tree`;
        let fullPath = `${url}?ref=${branch}&path=${path}`; // &recursive=true
        let tree = JSON.parse(await fetchStaticAsString(fullPath));
        console.log(tree);  // TODO(JBR) contains HTML error when it fails, we'll have to deal it.
        return tree.filter((x) => x['type'] == urlInfos['type']);
    }

    /**
     * Render tree in the inner HTML.
     */
    renderTree(tree)
    {
        // TODO(JBR) render tree
        let dirNav= this.querySelector('nav.directory ul');
        let lines = [];
        tree.forEach(item => {
            let link = route.buildButton('#/' + item.path, item.name, 'dark');
            let line = document.createElement('li');
            //line.innerHTML = item.name;
            line.append(link);
            lines.push(line);
        });
        lines.forEach(line => dirNav.appendChild(line));
    }

    /**
     * Update the state for host info.
     */
    updatePathInfo(key, val)
    {
        let info = this.stateGet('path-info');
        if (!info)
        {
            info = {};
        }
        info[key] = val;
        let keys = Object.keys(info);
        this.stateDispatch('path-info', info);
    }

    render()
    {
        let id = this.id;
        this.stateRegister(['path-info']);
        this.stateTrigger(
            'path-info',
            (kvs) => {
                if (kvs &&
                    FdsGitlabTree.observedAttributes.every(
                        (k) => Object.keys(kvs).indexOf(k) >= 0 && kvs[k])
                )
                {
                    console.log('Get the GitlabTree with: ', kvs);
                    this.getGitlabTree(kvs).then((tree) => this.renderTree(tree));
                }
            }
        );
        FdsGitlabTree.observedAttributes.forEach(key => {
            this.stateTrigger(
                key,
                (val) => {
                    if (val) this.updatePathInfo(key, val);
                }
            );
        });
    }

    static tagName = 'fds-gitlab-tree';

    /** default attributes with default value. */
    static defaultAttributes = {
        // Host of the repository to show on this custom element.
        'repo-host': undefined,
        // Project id or name of the repository.
        'project-id': undefined,
        // Which branch to check on the repository (default: master).
        'branch': 'master',
        // The path to check on the repository (default: 'articles').
        'path': 'articles',
        // The type of files to filter on, either 'tree' or 'blob' (default: 'tree').
        'type': 'tree'
    };
}

export { FdsGitlabTree };
