#! /bin/bash

DIRNAME=${CI_PROJECT_DIR:=/opt/faeries.space}

if [ -d $DIRNAME ]
then
    mkdir $DIRNAME/ci/nginx/context/
    for dir in $DIRNAME/*
    do
        echo "Scanning $dir ..."
        if [ -d $dir ] && [ -d $dir/etc/nginx ]
        then
            echo "... Found Nginx configuration."
            # TODO(JBR) In the future, we'll need a way to ensure two projects
            # doesn't define the same config file.
            cp $dir/etc/nginx/*.conf $DIRNAME/ci/nginx/context/
        else
            echo "... No Nginx configuration found."
        fi
    done
else
    echo "Not in the Faeries Space... Nothing to do."
    exit 1
fi
