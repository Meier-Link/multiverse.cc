#! /bin/sh

# Script to be run to (re)create the services images used by Gitlab CI (among
# others).
# NB: Be careful, we're in 'sh' in our image.

registry="$(/sbin/ip route | awk '/default/ { print $3 }'):5000"

echo "Host registry available at ${registry}"

dind dockerd \
    --host=unix://var/run/docker.sock \
    --host=tcp://0.0.0.0:2375 \
    --insecure-registry ${registry} &

# TODO(JBR) Future improvement: make local registry secure.

DOCKER_TAG_PREFIX="${registry}/fvs-"
DIRNAME=${CI_PROJECT_DIR}
# TODO(JBN) Think of variable environment to ease the configuration
# i.e. which images to build for which stage
# To be able to enable only images we want to (re)build.
# PROJECTS="${PROJECTS:=dispatcher front}" # Should be got from environment
STAGES="build quality test deploy"  # TODO(JBR) add dev stage to build dev env.

echo "Running from ${DIRNAME}"

build_ci_image() {
    echo "Build image for $1"
    if [ -f $1/before_build.sh ]
    then
        ./$1/before_build.sh
    fi
    docker build -t "${DOCKER_TAG_PREFIX}$1:latest" ./$1/
    if [ -f $1/after_build.sh ]
    then
        ./$1/after_build.sh
    fi
    if [ $? -ne 0 ]; then exit 1; fi
}

push_ci_image() {
    echo "Push image for $1"
    if [ -f $1/before_push.sh ]
    then
        ./$1/before_push.sh
    fi
    docker push "${DOCKER_TAG_PREFIX}$1:latest"
    if [ -f $1/after_push.sh ]
    then
        ./$1/after_push.sh
    fi
    if [ $? -ne 0 ]; then exit 1; fi
}

build_image() {
    echo "Build image $1"
    docker build -t "${DOCKER_TAG_PREFIX}$1:latest" .
    if [ $? -ne 0 ]; then exit 1; fi
}

push_image() {
    echo "Push image $1"
    docker push "${DOCKER_TAG_PREFIX}$1:latest"
    if [ $? -ne 0 ]; then exit 1; fi
}

sleep 1
(
    cd $DIRNAME/ci/
    for d in *
    do
        if [ -d $d ]
        then
            build_ci_image $d
            push_ci_image $d
        fi
    done
)
echo "Projects to build for: $PROJECTS"
for project in ${PROJECTS}
do
    for stage in ${STAGES}
    do
        cur_dir=$DIRNAME/$project/docker/$stage
        if [ -d $cur_dir ]
        then
        (
            img_name="${project}-${stage}"
            cd $cur_dir
            build_image $img_name
            push_image $img_name
        )
        else
            echo "[WARNING] No $stage image found for project $project."
        fi
    done
done
