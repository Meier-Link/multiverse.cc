#!/bin/sh

registry="$(/sbin/ip route | awk '/default/ { print $3 }'):5000"

echo "Host registry available at ${registry}"

dind dockerd \
    --host=unix://var/run/docker.sock \
    --host=tcp://0.0.0.0:2375 \
    --insecure-registry ${registry} &

# TODO(JBR) Future improvement: make local registry secure.

if [ "$1" = "" ]
then
    echo "Missing name[:tag] or 'setup-images' for the image to build."
    exit 1
fi

BUILD_IMAGE=$1

attempts=3

while [ $attempts -gt 0 ]
do
    sleep 1
    attempts=$((attempts-1))
    docker build -t "${registry}/$BUILD_IMAGE" ./build && docker push "${registry}/$BUILD_IMAGE"
    if [ $? -eq 0 ]
    then
        exit 0
    fi
done
